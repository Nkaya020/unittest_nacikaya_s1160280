Dit is een doorsnee documentatie van tests wat ik heb uitgevoerd.
Dit zijn de hoofdlijnen.
Bij het project zijn er ook comments om het te verduidelijken.

VehicleTest.cs 
Deze functie test of er wanneer negatieve getallen gebruikt wordt een throw exception lanceert. 
Ik heb voor theory gekozen, omdat dan de waarde door mij beslist kan worden en eventueel meerdere erbij kan toevoegen. 
Als laatst heb ik geleverde project code veranderd, zodat de gebruiker geen negatieve of geen nummer achtige kan invoeren en automatisch teruggestuurd wordt tot het succes is.
Ook heb ik de test opnieuw uitgevoerd met succes.

PolicyHolderTest.cs 
Hier heb ik 2 dingen getest. 
1. Deze functie test of er wanneer negatieve getallen gebruikt wordt een throw exception lanceert.
Ik heb voor theory gekozen, omdat dan de waarde door mij beslist kan worden en eventueel meerdere erbij kan toevoegen.
Als laatst heb ik geleverde project code veranderd, zodat de gebruiker geen negatieve of geen nummer achtige kan invoeren en automatisch teruggestuurd wordt tot het succes is.
Ook heb ik de test opnieuw uitgevoerd met succes.
 
2. Deze functie test of er wanneer date verkeerd wordt ingevuld, in dit geval 5 cijfers bij geboortejaar ipv gewoonlijke 4.
Ik heb voor theory gekozen, omdat dan de waarde door mij beslist kan worden en eventueel meerdere erbij kan toevoegen. 
Als laatst heb ik geleverde project code veranderd, zodat de gebruiker geen verkeerde datum kan invoeren en automatisch teruggestuurd wordt tot het succes is. 
Ook heb ik de test opnieuw uitgevoerd met succes. 

PremiumCalculationTest.cs
Hier heb ik meerdere dingen getest.
1. Ik heb getest of die postcode naar behoren werkt.
Bij de testen heb ik het geschreven wat ik doe. 
Per range heb ik een test uitgevoerd, bijvoorbeeld tussen 1000 - 35xx etc. Data die ik hebt gebruikt is alles wat voorkomt uit orginele project, alleen double excepted toegevoed om een succesvole test uit te kunnen voeren.
Techniek is theory, zodat ik meerdere en eigen waardes kan invoeren. 
Wat fout was heb ik aangepast en vervolgens verbeterd.

2. Ik heb getest of no claim years, korting naar behoren werkt. 
De data die ik heb gebruikt is aantal jaren die ik vervolgens omgezet is naar double variabele. 
Ook waren er meerdere variables nodig om tot een calculation te komen, onder andere van policyholder, Vehicle.
Ik heb zowel jaarlijkse als maandelijks getest en succes. 
Techniek is theory, zodat ik meerdere en eigen waardes kan invoeren.
Wat fout was heb ik aangepast en vervolgens verbeterd.