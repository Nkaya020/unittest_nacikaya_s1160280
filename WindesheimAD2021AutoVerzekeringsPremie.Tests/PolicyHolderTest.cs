using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie.Tests
{
    public class PolicyHolderTest
    {
        [Theory]
        [InlineData(-18, "01/06/2011", "Age must be positive!")] // To test the case when you supply a negative age
        [InlineData(20, "25/09/73946", "Date format isn't valid")] // To test the case when invalid date is supplied
        public void Constructor_ShouldThrowExceptionWithMessage(int Age, string driverlicenseStartDate, string message)
        {

            var ex = Assert.Throws<Exception>(()=>
            {
                var policyHolder = new PolicyHolder(Age, driverlicenseStartDate, 1320, 3);
            });

            Assert.Equal(message, ex.Message);
        }
    }
}
