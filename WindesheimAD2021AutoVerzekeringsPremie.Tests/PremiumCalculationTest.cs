﻿using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;

namespace WindesheimAD2021AutoVerzekeringsPremie.Tests
{
    public class PremiumCalculationTest
    {

        // Look if risk surcharge is calculated based on zipcode
        [Theory]
        [InlineData(18, "09/10/2014", 1040, 2, 27.37)] // in zipcode range 10xx - 35xx 
        [InlineData(31, "09/10/2014", 3500, 2, 23.8)] // in zipcode 3500
        [InlineData(31, "09/10/2014", 3600, 2, 23.12)] // in zipcode 3600
        [InlineData(31, "09/10/2014", 3650, 2, 23.12)] // in zipcode range 36xx - 44xx 
        [InlineData(18, "09/10/2014", 4501, 2, 26.066666666666666)] // in zipcode over 4500
        public void PremiumAmountPerYear_ShouldHaveCorrectValue_ZipCode(int age, string DriversLicenseStartDate, int PostalCode, int NoClaimYears, double expected)
        {
            Vehicle vehicle = new Vehicle(200, 3000, 2019);
            PolicyHolder policyHolder = new PolicyHolder(age, DriversLicenseStartDate, PostalCode, NoClaimYears);
            PremiumCalculation premiumCalculation = new(vehicle, policyHolder, InsuranceCoverage.WA);

            double actual = premiumCalculation.PremiumAmountPerYear;

            Assert.Equal(expected, actual);
        }


        // Look if discount based on damage free years is calculated correctly
        [Theory]
        [InlineData(5, 23.8)] // base
        [InlineData(6, 22.61)] // 5% bracket
        [InlineData(8, 20.23)] // 15% bracket
        [InlineData(18, 8.33)] // 65% bracket (max)
        [InlineData(19, 8.33)] // 65% (doesnt go over max)
        public void PremiumAmountPerYear_ShouldHaveCorrectValue_Age(int noClaimYears, double expected)
        {
            var vehicle = new Vehicle(200, 3000, 2019);
            var policyHolder = new PolicyHolder(31, "09/10/2014", 1040, noClaimYears);
            var premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            var actual = premiumCalculation.PremiumAmountPerYear;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PremiumPaymentAmount_PaymentPeriodYear_ShouldReturnAmount()
        {
            var vehicle = new Vehicle(210, 4500, 1999);
            var paymentPeriod = PaymentPeriod.YEAR;
            var policyHolder = new PolicyHolder(35, "06/11/2014", 1045, 0);
            var premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.ALL_RISK);
            var expected = 44.39;


            var actual = premiumCalculation.PremiumPaymentAmount(paymentPeriod);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PremiumPaymentAmount_PaymentPeriodMonth_ShouldReturnAmount()
        {
            var vehicle = new Vehicle(210, 4500, 1999);
            var paymentPeriod = PaymentPeriod.MONTH;
            var policyHolder = new PolicyHolder(35, "06/11/2014", 1045, 0);
            var premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA_PLUS);
            var expected = 2.28;


            var actual = premiumCalculation.PremiumPaymentAmount(paymentPeriod);

            Assert.Equal(expected, actual);
        }

        // This test was failling befor multiplying by 1.0 and adding () parentheses
        [Fact]
        public void CalculateBasePremium_ShouldReturn_BaseAmount()
        {

            var vehicle = new Vehicle(200, 3000, 2011);
            var expected = 20; //This is calculated like this ((3000/100) - 10 +(200/5))/3


            var actual = PremiumCalculation.CalculateBasePremium(vehicle);


            Assert.Equal(expected, actual);
        }
    }
}
