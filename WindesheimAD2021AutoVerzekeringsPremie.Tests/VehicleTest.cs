﻿using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie.Tests
{
    public class VehicleTest
    {

        [Theory]
        [InlineData(200, 4000, 2019, 2)]
        [InlineData(300, 4000, 2022, 0)]
        public void Constructor_ShouldCalculateTheRightAge(int powerInKw, int valueInEuros, int constructionYear, double expected)
        {
            var vehicle = new Vehicle(powerInKw, valueInEuros, constructionYear);

            Assert.Equal(expected, vehicle.Age);
        }

        [Theory]
        [InlineData(-18, -1, -1, "PowerInKw must be positive")] // To test the case when you supply a negative number
        public void Constructor_ShouldThrowExcpWithMessage(int powerInKw, int valueInEuros, int constructionYear, string message)
        {

            var ex = Assert.Throws<Exception>(() =>
            {
                var vehicle = new Vehicle(powerInKw, valueInEuros, constructionYear);
            });

            Assert.Equal(message, ex.Message);
        }
    }
}